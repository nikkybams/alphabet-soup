package com.alphabetSoup.alphabetsoup.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.alphabetSoup.alphabetsoup.model.Location;
import com.alphabetSoup.alphabetsoup.model.SearchResultModel;

@Service
public class AlphabetSoupService {
    
    Logger logger = LoggerFactory.getLogger(AlphabetSoupService.class);

    private static final int NUM_ALPHABETS = 26;
    private List<Location> locations;

    public List<SearchResultModel> processInputFile(MultipartFile file) {
	BufferedReader buff;
	boolean isPart1read = false;
	boolean isPart2read = false;
	boolean isPart3read = false;
	String part1;
	int rowDim = 0;
	int colDim = 0;
	int minDim = 0;
	int maxDim = 0;
	int cnt = 0;
	String[][] grid = null;
	List<String> wordList = new ArrayList<String>();
	HashMap<String, List<Location>> alphaMap = new HashMap<String, List<Location>>(NUM_ALPHABETS);

	try {
	    String line;
	    InputStream ins = file.getInputStream();
	    buff = new BufferedReader(new InputStreamReader(ins));

	    while ((line = buff.readLine()) != null) {
		if (!isPart1read) {
		    part1 = line;
		    String[] arrayDimensions = part1.split("x");
		    rowDim = Integer.valueOf(arrayDimensions[0].trim());
		    colDim = Integer.valueOf(arrayDimensions[1].trim());
		    maxDim = colDim;
		    isPart1read = true;
		    continue;
		}
		if (!isPart2read) {
		    if (grid == null) {
			grid = new String[rowDim][colDim];
		    }
		    grid[cnt] = line.split(" ");
		    for (int j = 0; j < colDim; j++) {
			locations = alphaMap.get(grid[cnt][j]);
			if (null == locations) {
			    ArrayList<Location> loc = new ArrayList<Location>();
			    loc.add(new Location(Long.valueOf(cnt), Long.valueOf(j)));
			    alphaMap.put(grid[cnt][j], loc);
			} else {
			    locations.add(new Location(Long.valueOf(cnt), Long.valueOf(j)));
			}
		    }
		    cnt++;
		    if (cnt == rowDim) {
			isPart2read = true;
		    }
		    continue;
		}
		if (!isPart3read) {
		    wordList.add(line.trim());
		}

	    }
	} catch (IOException e) {
	    logger.error("Exception occurred in AlphabetSoupService.processInputFile",e);
	}

	return doWordSearch(grid, alphaMap, wordList, minDim, maxDim);
    }

    private List<SearchResultModel> doWordSearch(String[][] grid, HashMap<String, List<Location>> alphaMap,
	    List<String> wordList, int minDim, int maxDim) {
	List<SearchResultModel> resultList = new ArrayList<SearchResultModel>();

	for (String word : wordList) {
	    int word_length = word.length();
	    char[] letters = word.toCharArray();
	    String firstletter = String.valueOf(letters[0]);
	    List<Location> grid_locs = alphaMap.get(firstletter);
	    StringBuilder foundWord = new StringBuilder();
	    for (Location l : grid_locs) {
		int rootRow = l.getRow().intValue();
		int rootCol = l.getColumn().intValue();
		int endRow = 0;
		int endCol = 0;
		// find word WEST of current cell location
		String[] west = grid[rootRow];
		endRow = rootRow;
		for (int j = rootCol, x = 0; j >= minDim && x < word_length; j--, x++) {
		    foundWord.append(west[j]);
		    endCol = j;
		}
		if (word.equalsIgnoreCase(foundWord.toString())) {
		    resultList.add(
			    new SearchResultModel(word, l, new Location(Long.valueOf(endRow), Long.valueOf(endCol))));
		    break;
		} 

		// Find word North of current cell location
		foundWord = new StringBuilder();
		for (int j = rootRow, i = rootCol, x = 0; j >= minDim && x < word_length; j--, x++) {
		    foundWord.append(grid[j][i]);
		    endRow = j;
		    endCol = i;
		}
		if (word.equalsIgnoreCase(foundWord.toString())) {
		    resultList.add(
			    new SearchResultModel(word, l, new Location(Long.valueOf(endRow), Long.valueOf(endCol))));
		    break;
		} 

		// Find word EAST of current cell location 
		foundWord = new StringBuilder();
		String[] rrRow = grid[rootRow];
		endRow = rootRow;
		for (int j = rootCol, x = 0; j < maxDim && x < word_length; j++, x++) {
		    foundWord.append(rrRow[j]);
		    endCol = j;
		}
		if (word.equalsIgnoreCase(foundWord.toString())) {
		    resultList.add(
			    new SearchResultModel(word, l, new Location(Long.valueOf(endRow), Long.valueOf(endCol))));
		    break;
		} 

		// Find word SOUTH of current cell location  
		foundWord = new StringBuilder();
		for (int j = rootRow, i = rootCol, x = 0; j < maxDim && x < word_length; j++, x++) {
		    foundWord.append(grid[j][i]);
		    endRow = j;
		    endCol = i;
		}
		if (word.equalsIgnoreCase(foundWord.toString())) {
		    resultList.add(
			    new SearchResultModel(word, l, new Location(Long.valueOf(endRow), Long.valueOf(endCol))));
		    break;
		} 

		// Find word NORTH-WEST of current cell location
		foundWord = new StringBuilder();
		for (int j = rootRow, i = rootCol, x = 0; j >= minDim && i >= minDim
			&& x < word_length; j--, i--, x++) {
		    foundWord.append(grid[j][i]);
		    endRow = j;
		    endCol = i;
		}
		if (word.equalsIgnoreCase(foundWord.toString())) {
		    resultList.add(
			    new SearchResultModel(word, l, new Location(Long.valueOf(endRow), Long.valueOf(endCol))));
		    break;
		} 

		// Find word NORTH-EAST of current cell location 
		foundWord = new StringBuilder();
		for (int j = rootRow, i = rootCol, x = 0; j >= minDim && i < maxDim && x < word_length; j--, i++, x++) {
		    foundWord.append(grid[j][i]);
		    endRow = j;
		    endCol = i;
		}
		if (word.equalsIgnoreCase(foundWord.toString())) {
		    resultList.add(
			    new SearchResultModel(word, l, new Location(Long.valueOf(endRow), Long.valueOf(endCol))));
		    break;
		} 

		// Find word SOUTH-EAST of current cell location
		foundWord = new StringBuilder();
		for (int j = rootRow, i = rootCol, x = 0; j < maxDim && i < maxDim && x < word_length; j++, i++, x++) {
		    foundWord.append(grid[j][i]);
		    endRow = j;
		    endCol = i;
		}
		if (word.equalsIgnoreCase(foundWord.toString())) {
		    resultList.add(
			    new SearchResultModel(word, l, new Location(Long.valueOf(endRow), Long.valueOf(endCol))));
		    break;
		} 

		// Find word SOUTH-WEST of current cell location
		foundWord = new StringBuilder();
		for (int j = rootRow, i = rootCol, x = 0; j < maxDim && i >= minDim && x < word_length; j++, i--, x++) {
		    foundWord.append(grid[j][i]);
		    endRow = j;
		    endCol = i;
		}
		if (word.equalsIgnoreCase(foundWord.toString())) {
		    resultList.add(
			    new SearchResultModel(word, l, new Location(Long.valueOf(endRow), Long.valueOf(endCol))));
		    break;
		} 
	    }
	}

	return resultList;
    }
}
