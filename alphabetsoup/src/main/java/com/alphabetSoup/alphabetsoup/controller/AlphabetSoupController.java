package com.alphabetSoup.alphabetsoup.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.alphabetSoup.alphabetsoup.model.InputFileModel;
import com.alphabetSoup.alphabetsoup.model.SearchResultModel;
import com.alphabetSoup.alphabetsoup.service.AlphabetSoupService;

@Controller
public class AlphabetSoupController {

    @Autowired
    private AlphabetSoupService alphabetSoupService;

    @RequestMapping("/")
    public ModelAndView alphabetSoupMain() {
	ModelAndView view = new ModelAndView();
	view.setViewName("alphabetSoup");
	InputFileModel file = new InputFileModel();
	view.addObject("inputFileModel", file);
	return view;
    }

    @RequestMapping(value = "/processInputFile", method = RequestMethod.POST)
    public ModelAndView processInput(@ModelAttribute("inputFileModel") InputFileModel uploadFile,
	    BindingResult result) {
	ModelAndView view = new ModelAndView();
	view.setViewName("alphabetSoup");
	MultipartFile file = uploadFile.getFile();
	if (file.isEmpty()) {
	    view.addObject("screenMsg", "A file was not selected. Please select a file to upload");
	    return view;
	}
	List<SearchResultModel> resultList = alphabetSoupService.processInputFile(file);
	view.addObject("resultList", resultList);
	return view;
    }
}
