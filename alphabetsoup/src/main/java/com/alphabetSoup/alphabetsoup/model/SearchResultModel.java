package com.alphabetSoup.alphabetsoup.model;

public class SearchResultModel {
    private String word;
    private Location beginLocation;
    private Location endLocation;

    public SearchResultModel(String word, Location beginLocation, Location endLocation) {
	this.word = word;
	this.beginLocation = beginLocation;
	this.endLocation = endLocation;
    }

    public String getWord() {
	return word;
    }

    public void setWord(String word) {
	this.word = word;
    }

    public Location getBeginLocation() {
	return beginLocation;
    }

    public void setBeginLocation(Location beginLocation) {
	this.beginLocation = beginLocation;
    }

    public Location getEndLocation() {
	return endLocation;
    }

    public void setEndLocation(Location endLocation) {
	this.endLocation = endLocation;
    }

}
