package com.alphabetSoup.alphabetsoup.model;

import org.springframework.web.multipart.MultipartFile;

public class InputFileModel {
    private MultipartFile file;

    public MultipartFile getFile() {
	return file;
    }

    public void setFile(MultipartFile file) {
	this.file = file;
    }

}
