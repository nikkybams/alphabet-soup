package com.alphabetSoup.alphabetsoup.model;

public class Location {
    Long row;
    Long column;

    public Location(Long row, Long column) {
	this.row = row;
	this.column = column;
    }

    public Long getRow() {
	return this.row;
    }

    public Long getColumn() {
	return this.column;
    }

    public void setRow(Long aRow) {
	this.row = aRow;
    }

    public void setColumn(Long aColumn) {
	this.column = aColumn;
    }

    @Override
    public String toString() {
	return this.row + ":" + this.column;
    }
}
