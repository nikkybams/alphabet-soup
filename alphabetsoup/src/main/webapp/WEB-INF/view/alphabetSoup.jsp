<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<title>Alphabet Soup</title>
</head>
<body>
  <div class="container">
  <div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Alphabet Soup Challenge</h5>
        <p class="card-text"></p>
         <c:if test = "${not empty screenMsg}">
           <div class="alert alert-warning" role="alert">${screenMsg}</div>
         </c:if> 
         <c:if test="${empty resultList}">
         <div>
         <p>Please upload the input file.</p>
        <form modelAttribute="inputFileModel" id="fileUploadFrm" action="/processInputFile" enctype="multipart/form-data" method="post">
            <input type="file" name="file" id="file">
            <input type="submit" class="btn btn-primary" value="Submit">
        </form>
        </div>
        </c:if>
         <c:forEach items="${resultList}" var="resultItem" varStatus="cnt">
           ${resultItem.word} &nbsp; ${resultItem.beginLocation} &nbsp; ${resultItem.endLocation} <br>
         </c:forEach>
        <div>
        </div>
      </div>
    </div>
  </div>
 </div>
  </div>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>

</html>